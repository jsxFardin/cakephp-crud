<?php

App::uses('AppController', 'Controller');

class ProfilesController extends AppController
{

    function index()
    {
        $profils = $this->Profile->find('all');
        $this->set('profiles', $profils);
        // $this->Profiles->recursive = 0;
		// $this->set('profiles', $this->Paginator->paginate());
    }

    public function add()
    {
        // print_r($this->request->data);
        if ($this->request->is('post')) {
            $this->Profile->create();
            if ($this->Profile->save($this->request->data)) {
                $this->Flash->success(__('New Profile Added!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
    }
}
